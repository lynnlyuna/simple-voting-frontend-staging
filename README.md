**Implement a system with the following interfaces:**
1. A web system for people to express their opinions on candidates with modern frontend framework.

2. A valid HKID no. is required for the voting. An HKID no. allow voting ONLY ONE candidate for each campaign.

3. Able to host more than one voting campaign.

4. Each voting campaign with a start and end time.

5. A web interface for displaying the result after the end time.

6. The campaign will not accept new vote after the end time.

7. A web interface for displaying the current count/vote for each candidate.

8. A line-chart/histogram/pie-chart visualizing how the vote distributed.

9. Unit test cases to cover critical paths.

10. Avoid duplicate/concurrent voting by HKID.

11. A list to display all voting campaign.

12. display campaigns within start/end time first and order by total no. of votes.

13. display most recent ended campaign afterward.

# Requirement tools/environment
1. Node.js version 8.x or 10.x.

2. Angular CLI

## Development Fronend server
1. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
