process.env.CHROME_BIN = require('puppeteer').executablePath();

const { SpecReporter } = require('jasmine-spec-reporter');

const { JUnitXmlReporter } = require('jasmine-reporters');

const config = require('./protractor.conf').config;

config.capabilities = {
  browserName: 'chrome',
  chromeOptions: {
    binary: process.env.CHROME_BIN,
    args: ['--headless', '--no-sandbox', '--disable-gpu', '--window-size=411,823', '--disable-web-security',
    '--disable-dev-shm-usage']
  },
};

config.framework = 'jasmine2';

config.onPrepare = () => {
  require('ts-node').register({
    project: 'e2e/tsconfig.e2e.json'
  });
  jasmine.getEnv().addReporter(new JUnitXmlReporter({
    consolidateAll: true,
    savePath: '.',
    filePrefix: 'junit-e2e'
  }));
  jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
}

exports.config = config;
