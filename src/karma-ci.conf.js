const baseConf = require('./karma-base.conf.js');

module.exports = function (config) {
  const conf = baseConf.get(config);

  config.set({
    ...conf,
    junitReporter: {
      outputDir: '..',
      outputFile: 'junit-karma.xml',
      useBrowserName: false,
    },
    jasmineDiffReporter: {
      pretty: true,
      color: {
        warningFg: 'black'
      }
    },
    mochaReporter: {
    },
    plugins: [
      ...conf.plugins
    ],
    singleRun: true
  });
};
