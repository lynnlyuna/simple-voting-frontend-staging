import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.css'],
})
export class CountComponent implements OnInit, OnDestroy {

  private sub: any;

  campaign: any = { answer: [], question: '', question_id: 0 };
  voteCount: any = [];
  voted: any = { hkid: '', answer: '', question_id: 0 };

  constructor(
    public api: ApiService,
    private route: ActivatedRoute,
    private _ngzone: NgZone,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.api.getCampaignByID(params['id']).subscribe((data: object) => {
        setTimeout(() => {
          this._ngzone.run(() => {
            if (data['statusCode'] === 200) {
              this.campaign = data['data'];

              // Get Count
              this.campaign['answer'].forEach((element) => {
                this.voteCount[element] = 0;
              });
              this.api.getVoteCount(params['id']).subscribe((_voteCount: object) => {
                this.voted = JSON.parse(localStorage.getItem('voted'));
                if (_voteCount['statusCode'] === 200) {
                  this.voteCount = Object.assign({}, this.voteCount, _voteCount['data']);
                }
              });
            }
          });
        });
      });
      // In a real app: dispatch action to load the details here.
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
