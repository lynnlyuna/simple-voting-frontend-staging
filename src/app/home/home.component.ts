import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  CampaignList: [];

  constructor(public api: ApiService, private router: Router, private _ngzone: NgZone) { }

  ngOnInit() {
    this.api.getListCampaignList().subscribe((data: object) => {
      setTimeout(() => {
        this._ngzone.run(() => {
          if (data['statusCode'] === 200) {
            this.CampaignList = data['data'];
          }
        });
      });
    });
  }
}
