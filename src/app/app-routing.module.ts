import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignComponent } from './campaign/campaign.component';
import { ResultComponent } from './result/result.component';
import { HomeComponent } from './home/home.component';
import { CountComponent } from './count/count.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'campaign/:id', component: CampaignComponent },
  { path: 'result/:id', component: ResultComponent },
  { path: 'count/:id', component: CountComponent },
  { path: 'result/:id', component: ResultComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
