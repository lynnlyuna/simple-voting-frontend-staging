import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  url = 'http://localhost:3000/';

  constructor(private _http: HttpClient) { }

  tryParseInt(val, default_val) {
    if (/^\d+$/g.test(val)) {
      return parseInt(val, 10);
    }
    return default_val;
  }

  // a and b are javascript Date objects
  dateDiffInDays(_a, _b) {
    const a = _a === '' ? new Date() : new Date(_a);
    const b = _b === '' ? new Date() : new Date(_b);
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY) + 1;
  }

  getListCampaignList() {
    return this._http.get(`${this.url}campaign-list/`, httpOptions);
  }

  getCampaignByID(id: string) {
    return this._http.post(`${this.url}campaign/${id}`, {}, httpOptions);
  }

  saveVoting(data: any) {
    return this._http.post(`${this.url}vote/create/`, data, httpOptions);
  }

  getVoteCount(question_id: string) {
    return this._http.post(`${this.url}vote/count/`, { question_id: this.tryParseInt(question_id, 0) }, httpOptions);
  }
}
