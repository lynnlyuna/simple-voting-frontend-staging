import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css'],
})
export class CampaignComponent implements OnInit, OnDestroy {

  private sub: any;

  campaign: any = { answer: [], question: '', question_id: 0 };

  registerForm: FormGroup;
  submitted = false;
  started = false;

  constructor(
    public api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _ngzone: NgZone,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      answer: ['', Validators.required],
      hkid: ['', Validators.pattern('^[A-MP-Za-mp-z]{1,2}[0-9]{6}[0-9A]')],
    });

    this.sub = this.route.params.subscribe((params) => {
      this.api.getCampaignByID(params['id']).subscribe((data: object) => {
        setTimeout(() => {
          this._ngzone.run(() => {
            this.started = false;
            if (data['statusCode'] === 200) {
              this.started = true;
              this.campaign = data['data'];
            } else if (data['error'] === 'Not yet start') {
              this.campaign = data['data'];
              this.started = false;
            } else if (data['error'] === 'Voting ended') {
              this.started = false;
              this.router.navigate([`/result/${data['data']['question_id']}`]);
            } else {
              this.router.navigate(['/']);
            }
          });
        });
      });
      // In a real app: dispatch action to load the details here.
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    // Save Voting
    const voted = Object.assign({}, this.registerForm.value, { question_id: this.api.tryParseInt(this.campaign.question_id, 0) });
    this.api.saveVoting(voted).subscribe((data: object) => {
      setTimeout(() => {
        this._ngzone.run(() => {
          if (data['statusCode'] === 200) {
            localStorage.setItem('voted', JSON.stringify(voted));
            this.router.navigate([`/count/${this.campaign.question_id}`]);
          } else if (data['error'] === 'Duplicate HKID') {
            alert('Duplicate voting by HKID');
            voted['answer'] = data['answer'];
            localStorage.setItem('voted', JSON.stringify(voted));
            this.router.navigate([`/count/${this.campaign.question_id}`]);
          } else {
            alert('Please try again');
          }
        });
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
