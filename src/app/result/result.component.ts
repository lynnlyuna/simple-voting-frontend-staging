import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

declare const Highcharts: any;

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
})
export class ResultComponent implements OnInit, OnDestroy {

  private sub: any;

  campaign: any = { answer: [], question: '', question_id: 0 };
  voteCount: any = [];

  constructor(
    public api: ApiService,
    private route: ActivatedRoute,
    private _ngzone: NgZone,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.api.getCampaignByID(params['id']).subscribe((data: object) => {
        setTimeout(() => {
          this._ngzone.run(() => {
            if (data['statusCode'] === 200 || data['statusCode'] === 201) {
              this.campaign = data['data'];

              // Get Count
              this.campaign['answer'].forEach((element) => {
                this.voteCount[element] = 0;
              });
              this.api.getVoteCount(params['id']).subscribe((_voteCount: object) => {
                if (_voteCount['statusCode'] === 200) {
                  this.voteCount = Object.assign({}, this.voteCount, _voteCount['data']);

                  // Vote Persentage
                  const pie_data = [];
                  const total_val = Object.keys(this.voteCount).reduce((sum, key) => sum + parseFloat(this.voteCount[key] || 0), 0);
                  Object.keys(this.voteCount).forEach((key) => {
                    pie_data.push({ name: key, y: this.voteCount[key] });
                  });
                  this.setChart(this.campaign['question'], pie_data);
                }
              });
            }
          });
        });
      });
      // In a real app: dispatch action to load the details here.
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  setChart(title, data) {
    Highcharts.chart('container', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
      },
      credits: {
        enabled: false,
      },
      title: {
        text: title,
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
            },
          },
        },
      },
      series: [{
        data,
        name: 'Voted',
        colorByPoint: true,
      }],
    });
  }

}
