// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html
const baseConf = require('./karma-base.conf.js');

module.exports = function(config) {
  config.set(baseConf.get(config));
};
